<?php 
	require('config.php');
	$user = $_SESSION['user'];
?>

<!DOCTYPE html>
<html>
  <head>
	  <?php require('myheader.php'); ?>
  </head>
  <body>
		<?php 
			require('navbar.php'); ?>
		  <script>
				document.getElementById("li-dashboard").className = "";
				document.getElementById("li-discover").className = "";
				document.getElementById("li-dropdown").className = "active";
		  </script>
			
			<div class="container" style="padding-top:80px;">
				
				<form method="post" action="./php/updateuser.php" class="form-horizontal">
			        <fieldset>
				        <input type="hidden" name="inputPrevEmail" value="<?php echo $user; ?>">
			          <div class="form-group">
                	<label for="inputEmail" class="col-lg-4 control-label">Email</label>

									<div class="col-lg-8">
                  	<input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder=".EDU Email" value="<?php echo $user; ?>">
                	</div>
              	</div>
								<div class="form-group">
                	<label for="inputPassword" class="col-lg-4 control-label">Password</label>

									<div class="col-lg-8">
                  	<input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Password">
                	</div>
              	</div>
								<div class="form-group">
                	<label for="inputPasswordConfirm" class="col-lg-4 control-label">Password Confirm</label>

									<div class="col-lg-8">
                  	<input type="password" class="form-control" id="inputPasswordConfirm" placeholder="Password Confirm">
                	</div>
              	</div>
              	
              	<div class="form-group">
                	<label for="inputName" class="col-lg-4 control-label">Nickname</label>

									<div class="col-lg-8">
                  	<input type="plaintext" class="form-control" name="inputName" id="inputName" placeholder="John, FlashingCat, etc..." value="<?php echo explode("@",$user)[0];?>" >
                	</div>
              	</div>
              	<?php
	              	// Connect to the server and select the database
									$connection = mysqli_connect("$host","$dbusername","$dbpassword",$dbname);
									if (mysqli_connect_errno()) {
									        echo "Failed to connect to MySQL: " . mysqli_connect_error();
									        exit();
									}
									
	              	$result = mysqli_query($connection,"SELECT * FROM `username` WHERE username='$user'");
									$row = mysqli_fetch_object($result);
									$talents = explode(":::",$row->talents);
	              	
	              	// LOL HACKATHON AT ITS FINEST.
	             	?>
              	<div class="form-group">
	              	<label for="inputTalents" class="col-lg-12 control-label" style="text-align: center">Talents</label>
	              	<div class="col-lg-12">
						        <div class="checkbox" style="padding-top:20px">
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-web-front" <? if(in_array("dev-web-front", $talents)) echo "checked";?>> Web Development: Front-End
						          </label><br>
											<label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-web-back" <? if(in_array("dev-web-back", $talents)) echo "checked";?>> Web Development: Back-End
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-mobile" <? if(in_array("dev-mobile", $talents)) echo "checked";?>> Mobile Development (iOS/Android/Windows)
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-win" <? if(in_array("dev-win", $talents)) echo "checked";?>> Windows Application Development
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-osx" <? if(in_array("dev-osx", $talents)) echo "checked";?>> OSX Application Development
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-music" <? if(in_array("art-music", $talents)) echo "checked";?>> Music
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-design-uiux" <? if(in_array("art-design-uiux", $talents)) echo "checked";?>> Product Design: UI/UX
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-design-graphics" <? if(in_array("art-design-graphics", $talents)) echo "checked";?>> Product Design: Graphics (Photoshop/Illustrator/etc)
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="marekting" <? if(in_array("marekting", $talents)) echo "checked";?>> Marketing
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="statistics" <? if(in_array("statistics", $talents)) echo "checked";?>> Statistics
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="management" <? if(in_array("management", $talents)) echo "checked";?>> Management
						          </label><br>
						        </div>
	              	</div>
			        </fieldset>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-primary">UPDATE</button>
		      </div>
		      </form>
				
				<?php 
					// Close the connection to MySQL
					mysqli_close($connection);
				?>
				
			</div>
	
		<?php
    require('myfooter.php'); ?>
  </body>
</html>