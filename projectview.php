<?php 
	require('config.php');
	$project_id = $_GET['proj_id'];
	$project_name = $_GET['n'];
?>

<!DOCTYPE html>
<html>
  <head>
	  <?php require('myheader.php'); ?>
  </head>
  <body>	  
	  <div id="wrapper">
				<?php 
			require('navbar.php'); ?>
			<script>
		  document.getElementById("navbar-brand").innerHTML = "<i class='mdi-av-play-arrow'></i>";
		  document.getElementById("navbar-brand").href = "#menu-toggle";
		</script>
        <!-- Sidebar -->
        <div id="sidebar-wrapper" class="shadow-z-2">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        <?php echo $project_name; ?>
                    </a>
                </li>
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    <a href="#">Calender</a>
                </li>
                <li>
                    <a href="#">Message</a>
                </li>
                <li>
                    <a href="#">Files</a>
                </li>
            </ul>
            <ul class="sidebar-nav" id="setting">
                <li>
                    <a href="#" style="position: fixed; bottom:50px; width: 250px;">Settings</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper" style="width:95%;">
          <div class="container-fluid" style="padding-top: 60px;">
            <div class="row">
              <div class="col-lg-12">
                <div class="panel panel-warning">
								  <div class="panel-heading">
								    <h3 class="panel-title">2 new messages</h3>
								  </div>
								  <div class="panel-body">
									  <div class="row">
										  <div class="col-sm-2">
											  <img src="img/sample_john.png">
										  </div>
										  <div class="col-sm-9" style="padding-top:15px;"> 
											  <span> John: Hey, I refined some idea. Check it out... </span>
										  </div>
										  <div class="col-sm-1" style="padding-top:15px;"> 
											  <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right:20px;">×</button>
										  </div>
									  </div>
									  <hr>
									  <div class="row">
										  <div class="col-sm-2">
											  <img src="img/sample_Quezia.png">
										  </div>
										  <div class="col-sm-9" style="margin-top:15px;"> 
											  <span> Quezia: Uploaded the music you requested! </span>
										  </div>
										  <div class="col-sm-1" style="padding-top:15px;"> 
											  <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right:20px;">×</button>
										  </div>
									  </div>
								  </div>
								</div>      
              </div>
          	</div> <!--row-->
          	<div class="row">
	          	<div class="col-md-4">
		          	<div class="panel panel-primary">
			          	<div class="panel-heading">
				          	<h3 class="panel-title"> TODO </h3>
			          	</div>
			          	
			          	<div class="panel-body">
									  <div class="row">
										  <div class="col-xs-12">
										    <div class="checkbox">
						              <label>
						                <input type="checkbox"> invite Harry
						              </label>
						            </div>
										  </div>
									  </div>
									  <div class="row">
										  <div class="col-xs-12">
										    <div class="checkbox">
						              <label>
						                <input type="checkbox" checked> <span style="text-decoration: line-through">email Ming</span>
						              </label>
						            </div>
										  </div>
									  </div>
									  <div class="row">
										  <div class="col-xs-12">
										    <div class="checkbox">
						              <label>
						                <input type="checkbox" checked> <span style="text-decoration: line-through">upload project description</span>
						              </label>
						            </div>
										  </div>
									  </div>
									</div>
		          	</div>
	          	</div> <!-- col-md-4-->
	          	
	          	<div class="col-md-4">
		          	<div class="panel sample_calendar" style="width:350px; height:300px;"></div>
		          </div> <!-- col-md-4-->	
		          
		          <div class="col-md-4">
		          	<div class="panel sample_task_manager" style="width:335px; height:144px;"></div>
		          </div> <!-- col-md-4-->
          	</div> <!--row-->
      		</div>
			  </div><!-- /#page-content-wrapper -->
			</div><!-- /#wrapper -->
		
		<?php
    require('myfooter.php'); ?>
    
    <!-- Menu Toggle Script -->
    <script>
    $(".navbar-brand").click(function(){
    	$(this).toggleClass("down"); 
		});
		
    $("#navbar-brand").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
  </body>
</html>