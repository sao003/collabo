#PRJT
## Inspiration
You know how to code and have cool idea, but your app looks really bad. Turns out you should have a designer's help, but you are a student, so you can't afford one. You go find your friend's friend's friend's friend's who is artist. You lost her contact somehow so you have to do this all over again.......

**STOP THIS MESS**

This is not just one example of college students with skills, idea, and motivation. There are a lot of SNS these days, but ultimately they are for socializing. 


## What it does
**PRJT** is a tool that helps _finding people_ in professional manner.

- You can sign up with your skillset, and create a project. Add the talents you need to get your project going. PRJT will broadcast your project to people who have the talents you need. 

- While people sign up, add some ideas to TODO list and add some due dates to the calendar.

- When people join, say hi to them using embedded **chat** * system.

- Upload files and share them with your group *.

No idea, but you have some urge to do something awesome? No problem.

**PRJT** also lets you _find projects_.

Just browse DISCOVER tab to:

- View recently added projects 

- All projects

- Hot projects

- Projects that matches your skill

## How I built it
This prototype webapp was built at HackUCI alone using Twitter Bootstrap, Google Material Design, MAMP (Mac OSX, Apache2, MySQL DB, PHP) Stack, CODA2 IDE. 

## Challenges I ran into
~~Sleep Deprived~~ Some fundamental challenges like designing table schema, technical challenges like javascript syntax, and maybe hunger..?

## What's next for PRJT
Within timeframe, only fundamental functionalities such as "login", "signup", "create a project", "join project" have been implemented. It may sound simple (as they only count 4), but they are indeed more than a thousand lines of code with some serious schematic designs for optimized and scalable application.

I'm planning on taking this further to finish up all the functionalities I have mentioned above, and polish business idea with mentors. I've been asking this idea to random students around my beautiful campus UCSD, and I'm almost certain that students would enjoy this. (Still need some real data like survey, etc)

to be or not to be that is the question
can i make changes?
adfadf
