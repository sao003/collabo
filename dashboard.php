<?php 
	require('config.php');
	require('php/project.php');
?>

<!doctype html>
<html>
	<head>
		<?php require('myheader.php'); ?>
	</head>
  <body>
<?php 
	require('navbar.php');
?>
	  <script>
			document.getElementById("li-dashboard").className = "active";
			document.getElementById("li-discover").className = "";
			document.getElementById("li-dropdown").className = "";
		</script>
		
			
		<?php
			$proj = new Project();
			$connection = $proj->init();
			if ($proj->getMyProjectCount($connection) == 0 && $proj->getCreatedProjectCount($connection) == 0) {
			?>
			<div class="container" style="padding-top:100px;">
			  <h2> Wait.. You don't have a project! </h2>
			  
			  <div style="text-align: center; padding-top:100px;">
				  <button type="button" data-toggle="modal" data-target="#create-project-modal" class="btn btn-primary btn-lg">CREATE A PROJECT</button> <br>
				  <span style="font-size: 150%;"> or </span> <br>
				  <a href="discover.php" class="btn btn-primary btn-lg">DISCOVER ONE</a>
					</div>
				</div>				
		<?php } else { ?>
						<div class="container" style="margin-top: 100px;">
							<h2 style="padding-bottom:20px;"> My Projects </h2>
							<div class="row" style="padding-top: 10px;">
								<div class='col-md-4' style='height:500px; max-height:500px;'>
							    <div class='panel panel-default shadow-z-3' style='height:460px;'>
									  <div class='panel-body' style='text-align:center; width:100%; height:100%; padding:0'>
										  <a class='btn mdi-image-control-point' data-toggle="modal" data-target="#create-project-modal" style='font-size:1000%; width:100%; height:100%; padding-top:150px;'></a>
									  </div>
							    </div>
								</div>
						<?php
						$proj = new Project();
						$connection = $proj->init();
						$count = $proj->getProjectCount($connection);
						$userid = $_SESSION['userid'];
						
						$myProjArr = array();
						
						$result = mysqli_query($connection,"SELECT * FROM projects WHERE owner_id=$userid");
						while ($row = mysqli_fetch_object($result)) {
							$myProjArr[] = $row->id;
							
							$talents = explode(":::",$row->talents_needed);
					    echo "
					    <div class='col-sm-4' style='height:500px; max-height:500px;'>
					    <div class='panel panel-primary'>
							  <div class='panel-heading'>
							    <h3 class='panel-title' style='font-size:150%;'>".$row->projectname."</h3>
							  </div>
							  <div class='panel-body shadow-z-3' style='height:420px;'>
							  	<div class='row'>
							  		<div class='col-sm-12'> 
							  		<span><b>Project Description:</b></span> <br>
							  		<div style='overflow-y:auto; height:110px; outline: 1px solid green; padding:5px;'>
										<span style='padding-left:1em'>".$row->projectdesc."</span>
										</div> <br><br>
										<span><b>Talents Needed:</b></span> <br>
										<div style='overflow-y:auto; overflow-x:hidden; height:90px; outline: 1px solid green; padding-left:4px;'>
										<div class='row'>
										
										";
										
										foreach (explode(":::",$row->talents_needed) as $val) {
																												
											echo "<div class='col-sm-12'><span class='label label-info'>".$proj->translateProjects($val)."</span></div>";
										}
										echo " </div></div>
										</div>
									</div> <br> <hr>
									<div class='row'>
									<div class='col-md-12'><a href='projectview.php?proj_id=".$row->id."&n=".$row->projectname."' class='btn btn-primary shadow-z-1' style='width:100%;'> VIEW </a> </div></div>
							  </div>
							</div>
							</div>";
						}
					?>
					</div><!--row-->
					
					<hr>
					<h2 style="padding-bottom:20px;"> Projects You Are In </h2>
					<div class="row">
						<?php
							$result = mysqli_query($connection,"SELECT * FROM members WHERE user_id=$userid");
							$assoc_projects = array();
							while ($row = mysqli_fetch_object($result)) { 
								$assoc_projects[] = $row->project_id;
							}
							foreach($assoc_projects as $projects) {
								$result2 = mysqli_query($connection,"SELECT * FROM projects WHERE id=$projects LIMIT 1");
								$found_proj = mysqli_fetch_object($result2);
								$talents = explode(":::",$found_proj->talents_needed);
						    echo "
						    <div class='col-sm-4' style='height:500px; max-height:500px;'>
						    <div class='panel panel-primary'>
								  <div class='panel-heading'>
								    <h3 class='panel-title' style='font-size:150%;'>".$found_proj->projectname."</h3>
								  </div>
								  <div class='panel-body shadow-z-3' style='height:420px;'>
								  	<div class='row'>
								  		<div class='col-sm-12'> 
								  		<span><b>Project Description:</b></span> <br>
								  		<div style='overflow-y:auto; height:110px; outline: 1px solid green; padding:5px;'>
											<span style='padding-left:1em'>".$found_proj->projectdesc."</span>
											</div> <br><br>
											<span><b>Talents Needed:</b></span> <br>
											<div style='overflow-y:auto; overflow-x:hidden; height:90px; outline: 1px solid green; padding-left:4px;'>
											<div class='row'>
											
											";
											
											foreach (explode(":::",$found_proj->talents_needed) as $val) {
												echo "<div class='col-sm-12'><span class='label label-info'>".$proj->translateProjects($val)."</span></div>";
											}
											echo " </div></div>
											</div>
										</div> <br> <hr>
										<div class='row'>
										<div class='col-md-12'><a href='projectview.php?proj_id=".$found_proj->id."&n=".$found_proj->projectname."' class='btn btn-primary shadow-z-1' style='width:100%;'> VIEW </a> </div></div>
								  </div>
								</div>
								</div>";
							}
						?>
					</div>
				</div> <!--container-->
				
			<?php
		} 
			?>
			
				<!-- add project modal -->
    <div id="create-project-modal" class="modal signup-modal">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		        <h4 class="modal-title">CREATE A PROJECT</h4>
		      </div>
		      <div class="modal-body">  
		        <form method="post" action="./php/CreateProj.php" class="form-horizontal">
			        <fieldset>
			          <div class="form-group">
                	<label for="inputProjName" class="col-lg-4 control-label">Project Name</label>

									<div class="col-lg-8">
                  	<input type="plaintext" class="form-control" name="inputProjName" id="inputProjName">
                	</div>
              	</div>
								<div class="form-group">
                	<label for="inputProjDesc" class="col-lg-4 control-label">Description</label>

									<div class="col-lg-8">
                  	<textarea type="plaintext" class="form-control" name="inputProjDesc" id="inputProjDesc" placeholder="Talk about your project"></textarea>
                	</div>
              	</div>
								              	
              	<div class="form-group">
	              	<label for="inputTalents" class="col-lg-12 control-label" style="text-align: center">Needed Talents</label>
	              	<div class="col-lg-12">
						        <div class="checkbox" style="padding-top:20px">
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-web-front"> Web Development: Front-End
						          </label><br>
											<label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-web-back"> Web Development: Back-End
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-mobile"> Mobile Development (iOS/Android/Windows)
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-win"> Windows Application Development
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-osx"> OSX Application Development
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-music"> Music
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-design-uiux"> Product Design: UI/UX
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-design-graphics"> Product Design: Graphics (Photoshop/Illustrator/etc)
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="marekting"> Marketing
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="statistics"> Statistics
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="management"> Management
						          </label><br>
						        </div>
	              	</div>
	              </div>
			        </fieldset>
		      </div>
		      
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
		        <button type="submit" class="btn btn-primary">CREATE</button>
		      </div>
		      </form>
		      
		    </div>
		  </div>
		</div>
			
			<?php
			$proj->disconnectDB($connection);
		
		require('myfooter.php'); ?>
  </body>
</html>