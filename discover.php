<?php 
	require('config.php');
	require('php/project.php'); 
?>

<!doctype html>
<html>
	<head>
		<?php require('myheader.php'); ?>
	</head>
  <body>
<?php 
	require('navbar.php'); ?>
	<div class="container" style="margin-top: 100px;">
		<h2 style="padding-bottom:20px;"> Recent Projects </h2>
		<div class="row" style="padding-top: 10px;">
			
	<?php
	$proj = new Project();
	$connection = $proj->init();
	$count = $proj->getProjectCount($connection);
	$n = 0;
	$result = mysqli_query($connection,"SELECT * FROM projects");
	while ($row = mysqli_fetch_object($result)) {
		$talents = explode(":::",$row->talents_needed);

    echo "
    <div class='col-sm-4' style='height:500px; max-height:500px;'>
    <div class='panel panel-primary'>
		  <div class='panel-heading'>
		    <h3 class='panel-title' style='font-size:150%;'>".$row->projectname."</h3>
		  </div>
		  <div class='panel-body shadow-z-3' style='height:420px;'>
		  	<div class='row'>
		  		<div class='col-sm-12'> 
		  		<span><b>Project Description:</b></span> <br>
					<div style='overflow-y:auto; height:110px; outline: 1px solid green; padding:5px;'>
					<span style='padding-left:1em;'>".$row->projectdesc."</span> 
					</div> <br><br>
					<span><b>Talents Needed:</b></span> <br>
					<div style='overflow-y:auto; overflow-x:hidden; height:90px; outline: 1px solid green; padding-left:4px;'>
					<div class='row'>
					
					";
					
					foreach (explode(":::",$row->talents_needed) as $val) {
																							
						echo "<div class='col-sm-12'><span class='label label-info'>".$proj->translateProjects($val)."</span></div>";
					}
					echo " </div></div>
					</div>
				</div> <br> <hr>
				<div class='row'>
				<div class='col-md-12'>
				<form method='post' action='php/JoinProj.php'>
				<input type='hidden' name='proj_id' value='".$row->id."'></input>";
				
				if (!($proj->doIBelong($connection, $row->id, $_SESSION['userid']))) {
					echo "<button type='submit' class='btn btn-primary shadow-z-1' style='width:100%;'> JOIN </button>";
				} else {
					echo "<a href='projectview.php?proj_id=".$row->id."&n=".$row->projectname."' class='btn btn-success shadow-z-1' style='width:100%;'> VIEW </a>";
				} 
				echo "</form>
				 </div></div>
		  </div>
		</div>
		</div>";
		
	}
?>
		</div><!--row-->
	</div> <!--container-->


		<script>
			document.getElementById("li-dashboard").className = "";
			document.getElementById("li-discover").className = "active";
			document.getElementById("li-dropdown").className = "";
		</script>
	  <?php require('myfooter.php'); ?>
	  <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
  </body>
</html>