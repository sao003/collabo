<?php
	echo "<div id='top' class='navbar navbar-material-white shadow-z-2'>
      <div class='container-fluid'>
        <div class='navbar-header'>
          <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
          </button>
          <a id='navbar-brand' class='navbar-brand' href='#top'>PRJT</a>
        </div>
        <div class='navbar-collapse collapse navbar-responsive-collapse'>
          <ul class='nav navbar-nav'>
            <li id='li-dashboard' class='active'><a href='dashboard.php'>Dashboard</a></li>
            <li id='li-discover'><a href='discover.php'>Discover</a></li>
          </ul>
          <ul class='nav navbar-nav navbar-right'>
            <li id='li-dropdown' class='dropdown'>
              <a href='#' data-target='#' class='dropdown-toggle' data-toggle='dropdown'>".$_SESSION['user']."
                <b class='caret'></b></a>
              <ul class='dropdown-menu'>
                <li><a href='profile.php'>Profile</a></li>
                <li class='divider'></li>
                <li><a href='php/logout.php'>LOGOUT</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>";
    
?>