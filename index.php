<?php 
	require('config.php');
?>

<!DOCTYPE html>
<html>
  <head>
	  <?php require('myheader.php'); ?>
  </head>
  <body>
	  
	  <?php 
	  if (!isset($_SESSION['user'])) { ?>
    <div class="intro-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="intro-message">
              <h1>PRJT</h1>
              <h3>Get your idea into a project</h3>
              <hr class="intro-divider">
              <ul class="list-inline intro-social-buttons">
                <li>
                  <a href="http://devpost.com/software/prjt" class="btn btn-default btn-lg btn-raised" target="_blank"><span>ABOUT</span></a>
                </li>
                <li>
                  <a href="#" class="btn btn-default btn-lg btn-raised"><span>SHARE</span></a>
                </li>
                <li>
                  <a href="#" class="btn btn-default btn-lg btn-raised">DONATE</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-6">
	          <div class="login-module">
		          <form method="post" action="./php/login.php" class="form-horizontal">
			          <fieldset>
			          <div class="form-group">
                <label for="inputEmail" class="col-lg-4 control-label">Email</label>

                <div class="col-lg-8">
                  <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder=".EDU Email">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword" class="col-lg-4 control-label">Password</label>

                <div class="col-lg-8">
                  <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Password">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
	                <a href="#">forgot password?</a>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-info">LOGIN</button>
                  <button type="button" data-toggle="modal" data-target="#signup-modal" class="btn btn-primary">SIGN UP</button>
                </div>
              </div>
			          </fieldset>
		          </form>
	          </div>
	          
          </div>
        </div>
      </div>
    </div>
    </div>
    <!-- /.intro-header -->
    
    <!-- signup modal -->
    <div id="signup-modal" class="modal signup-modal">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		        <h4 class="modal-title">SIGN UP</h4>
		      </div>
		      <div class="modal-body">
		        
		        
		        <form method="post" action="./php/signup.php" class="form-horizontal">
			        <fieldset>
			          <div class="form-group">
                	<label for="inputEmail" class="col-lg-4 control-label">Email</label>

									<div class="col-lg-8">
                  	<input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder=".EDU Email">
                	</div>
              	</div>
								<div class="form-group">
                	<label for="inputPassword" class="col-lg-4 control-label">Password</label>

									<div class="col-lg-8">
                  	<input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Password">
                	</div>
              	</div>
								<div class="form-group">
                	<label for="inputPasswordConfirm" class="col-lg-4 control-label">Password Confirm</label>

									<div class="col-lg-8">
                  	<input type="password" class="form-control" id="inputPasswordConfirm" placeholder="Password Confirm" style="font-size:150%;">
                	</div>
              	</div>
              	
              	<div class="form-group">
                	<label for="inputName" class="col-lg-4 control-label">Nickname</label>

									<div class="col-lg-8">
                  	<input type="plaintext" class="form-control" name="inputName" id="inputName" placeholder="John, FlashingCat, etc...">
                	</div>
              	</div>
              	
              	<div class="form-group">
	              	<label for="inputTalents" class="col-lg-12 control-label" style="text-align: center">Talents</label>
	              	<div class="col-lg-12">
						        <div class="checkbox" style="padding-top:20px">
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-web-front"> Web Development: Front-End
						          </label><br>
											<label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-web-back"> Web Development: Back-End
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-mobile"> Mobile Development (iOS/Android/Windows)
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-win"> Windows Application Development
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="dev-osx"> OSX Application Development
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-music"> Music
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-design-uiux"> Product Design: UI/UX
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="art-design-graphics"> Product Design: Graphics (Photoshop/Illustrator/etc)
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="marekting"> Marketing
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="statistics"> Statistics
						          </label><br>
						          <label class="talent-chkbox">
						            <input type="checkbox" name="inputTalents[]" value="management"> Management
						          </label><br>
						        </div>
	              	</div>
			        </fieldset>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
		        <button type="submit" class="btn btn-primary">SIGN UP</button>
		      </div>
		      </form>
		    </div>
		  </div>
		</div>
		
		
    <?php }
	    else {  ?>
	    <script type="text/javascript">
            window.location.href = "dashboard.php";
      </script>
		    
		<?php 
	    }
		    
    require('myfooter.php'); ?>
  </body>
</html>
