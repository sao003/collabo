<?php
	require("../config.php");
	require('project.php');
	
	// Connect to the server and select the database
	$connection = mysqli_connect("$host","$dbusername","$dbpassword",$dbname);
	if (mysqli_connect_errno()) {
	        echo "Failed to connect to MySQL: " . mysqli_connect_error();
	        exit();
	}

	// Take data from the form
	$user_id = $_SESSION['userid'];
	$submitted_projname = $_POST['inputProjName'];
	$submitted_projdesc = $_POST['inputProjDesc'];
	$submitted_talents = $_POST['inputTalents'];
	
	$submitted_projname = stripslashes($submitted_projname);
	$submitted_projname = mysqli_real_escape_string($connection, $submitted_projname);
	
	$submitted_projdesc = str_replace( array("\n","\r","\r\n"), '<br />', $submitted_projdesc );
	$submitted_projdesc = stripslashes($submitted_projdesc);
	$submitted_projdesc = mysqli_real_escape_string($connection, $submitted_projdesc);


	$talents_string = "";
	$N = count($submitted_talents);
	for ($i = 0; $i < $N; $i++) {
		$talents_string = $talents_string.":::".$submitted_talents[$i];
	}
	$talents_string = preg_replace('/:::/','',$talents_string,1);
	
	mysqli_query ($connection,"set names utf8");
  //add to the database
  mysqli_query($connection, "INSERT INTO $projecttable (owner_id,projectname,projectdesc,talents_needed) VALUES ('$user_id','$submitted_projname','$submitted_projdesc','$talents_string')") or die(mysqli_error($connection));
  
  $proj = new Project();
/*
	$proj_id = mysqli_insert_id($connection);
	
	mysqli_query($connection, "INSERT INTO $membertable (project_id,user_id) VALUES ('$proj_id','$user_id')") or die(mysqli_error($connection));
*/
  mysqli_close($connection);

  $succ_message = "Project Successfully Added.";
  echo "<script type='text/javascript'>alert('$succ_message'); window.history.back();</script>";
?>