<?php
	require('../config.php');
	require('PasswordHash.php');
	
	// Connect to the server and select the database
	$connection = mysqli_connect("$host","$dbusername","$dbpassword",$dbname);
	if (mysqli_connect_errno()) {
	        echo "Failed to connect to MySQL: " . mysqli_connect_error();
	        exit();
	}

// Take data from the form
$previous_username = $_POST['inputPrevEmail'];
$submitted_username = $_POST['inputEmail'];
if ($_POST['inputPassword'] != "") {
	$submitted_password = create_hash($_POST['inputPassword']);
} else {
	$submitted_password = "";
}

$submitted_name = $_POST['inputName'];
$submitted_talents = $_POST['inputTalents'];

// Prevent sql injection
$previous_username = stripslashes($previous_username);
$submitted_username = stripslashes($submitted_username);
$submitted_password = stripslashes($submitted_password);
$previous_username = mysqli_real_escape_string($connection, $previous_username);
$submitted_username = mysqli_real_escape_string($connection, $submitted_username);
$submitted_password = mysqli_real_escape_string($connection, $submitted_password);

$talents_string = "";
$N = count($submitted_talents);
for ($i = 0; $i < $N; $i++) {
	$talents_string = $talents_string.":::".$submitted_talents[$i];
}
$talents_string = preg_replace('/:::/','',$talents_string,1);

if ($submitted_password != "") {
	$result = mysqli_query($connection, "UPDATE `username` SET username='$submitted_username', password = '$submitted_password', name = '$submitted_name', talents = '$talents_string' WHERE username='$previous_username'");
} else {
	$result = mysqli_query($connection, "UPDATE `username` SET username='$submitted_username', name = '$submitted_name', talents = '$talents_string' WHERE username='$previous_username'");
}
mysqli_close($connection);

$succ_message = "Update successful.";
echo "<script type='text/javascript'>alert('$succ_message'); window.history.back();</script>";

?>
