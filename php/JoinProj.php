<?php
	require('../config.php');
	require('project.php');
	
	$proj_id = $_POST['proj_id'];
	$user_id = $_SESSION['userid'];
	
	$proj = new Project();
	$connection = $proj->init();
	
	$proj->joinProject($connection, $proj_id, $user_id);
	$proj->disconnectDB($connection);
	header("Location: ../dashboard.php");
      die("Redirecting to: ../dashboard.php");
?>
	