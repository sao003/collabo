<?php
require("../config.php");
require("PasswordHash.php");

// Connect to the server and select the database
$connection = mysqli_connect("$host","$dbusername","$dbpassword",$dbname);
if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
}

// Take data from the form
$submitted_username = $_POST['inputEmail'];
$submitted_password = create_hash($_POST['inputPassword']);
$submitted_name = $_POST['inputName'];
$submitted_talents = $_POST['inputTalents'];

// Prevent sql injection
$submitted_username = stripslashes($submitted_username);
$submitted_password = stripslashes($submitted_password);
$submitted_username = mysqli_real_escape_string($connection, $submitted_username);
$submitted_password = mysqli_real_escape_string($connection, $submitted_password);

if ($stmt=mysqli_prepare($connection, "SELECT * FROM $usertable WHERE username=?")) {
  mysqli_stmt_bind_param($stmt, 's', $submitted_username);
  mysqli_stmt_execute($stmt);

	mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password, $name, $talents);

  mysqli_stmt_store_result($stmt);

  $count = mysqli_stmt_num_rows($stmt);

  // username was found
  if ($count == 1) {
	  echo "Email already exists!";
	} 
	
	// User can sign up
	else {
		$talents_string = "";
		$N = count($submitted_talents);
		for ($i = 0; $i < $N; $i++) {
			$talents_string = $talents_string.":::".$submitted_talents[$i];
		}
		$talents_string = preg_replace('/:::/','',$talents_string,1);
		
		mysqli_query ($connection,"set names utf8");
    //add to the database
    $add = mysqli_query($connection, "INSERT INTO $usertable (username,password,name,talents) VALUES ('$submitted_username','$submitted_password','$submitted_name','$talents_string')") or die(mysqli_error($connection));

    mysqli_close($connection);

    $succ_message = "Sign up successful. Please login again.";
    echo "<script type='text/javascript'>alert('$succ_message'); window.history.back();</script>";
	}
} else { echo "error";}
?>