<?php
//	require('config.php');
	require('Database.php');
	
	class Project {
		private $connection;
		
		public function init() {
			$connection = Database::connectDB();
			return $connection;
		}
		
		public function disconnectDB($con) {
			Database::closeConnection($con);
		}
		
		public function getMyProjectCount($con) {
			$userid = $_SESSION['userid'];
			
			$count = 0;

			if ($stmt= mysqli_prepare($con, "SELECT * FROM members WHERE user_id=?")) {
				mysqli_stmt_bind_param($stmt, 's', $userid);
				mysqli_stmt_execute($stmt);
				
				mysqli_stmt_bind_result($stmt, $id, $proj_id, $user_id);
				
				mysqli_stmt_store_result($stmt);
				
				$count = mysqli_stmt_num_rows($stmt);
				
				mysqli_stmt_close($stmt);
				
				return $count;
			}
		}
		
		public function getCreatedProjectCount($con) {
			$userid = $_SESSION['userid'];
			
			$count = 0;

			if ($stmt= mysqli_prepare($con, "SELECT * FROM projects WHERE owner_id=?")) {
				mysqli_stmt_bind_param($stmt, 's', $userid);
				mysqli_stmt_execute($stmt);
				
				mysqli_stmt_bind_result($stmt, $id, $owner_id, $projectname, $projectdesc,$talents);
				
				mysqli_stmt_store_result($stmt);
				
				$count = mysqli_stmt_num_rows($stmt);
				
				mysqli_stmt_close($stmt);
				
				return $count;
			}
		}
		
		public function getProjectCount($con) {
			$count = 0;

			$result = mysqli_query($con, "SELECT count(*) as count FROM projects") or die("error");
			$data = mysqli_fetch_assoc($result);
			$count = $data['count'];

			return $count;
		}
		
/*
		public function getProjectID($con) {
			$userid = $_SESSION['userid'];
			
			$proj_id = 0;

			if ($stmt= mysqli_prepare($con, "SELECT * FROM projects WHERE projectname=?")) {
				mysqli_stmt_bind_param($stmt, 's', $userid);
				mysqli_stmt_execute($stmt);
				
				mysqli_stmt_bind_result($stmt, $id, $proj_id, $user_id);
				
				mysqli_stmt_store_result($stmt);
				
				mysqli_stmt_close($stmt);
				
				return $proj_id;
			}
		}
*/
		
		public function joinProject($con, $proj_id, $user_id) {
			mysqli_query($con, "INSERT INTO `members` (project_id,user_id) VALUES ('$proj_id','$user_id')") or 
				die(mysqli_error($con));
		}
		
		public function doIBelong($con, $proj_id, $user_id) {
			$userid = $user_id;
			
			$count = 0;
			$count2 = 0;
			
			if ($stmt= mysqli_prepare($con, "SELECT * FROM projects WHERE id=$proj_id AND owner_id=?")) {
				mysqli_stmt_bind_param($stmt, 's', $userid);
				mysqli_stmt_execute($stmt);
				
				mysqli_stmt_bind_result($stmt, $id, $owner_id, $projectname, $projectdesc, $talents);
				
				mysqli_stmt_store_result($stmt);
				
				$count = mysqli_stmt_num_rows($stmt);
				
				mysqli_stmt_close($stmt);
				
				if ($count != 0) {
					return true;
				} else {
					if ($stmt2= mysqli_prepare($con, "SELECT * FROM members WHERE project_id=$proj_id AND user_id=?")) {
						mysqli_stmt_bind_param($stmt2, 's', $userid);
						mysqli_stmt_execute($stmt2);
						
						mysqli_stmt_bind_result($stmt2, $id, $proj_id, $user_id);
						
						mysqli_stmt_store_result($stmt2);
						
						$count2 = mysqli_stmt_num_rows($stmt2);
						
						mysqli_stmt_close($stmt2);
						
						if ($count2 != 0) {
							return true;
						} else {
							return false;
						}
					}
				}
			}
		}
		
		public function translateProjects($str) {
			$tal_human_readable = array('Web Development: Front-End', 'Web Development: Back-End', 
																						'Mobile Development (iOS/Android/Windows)', 'Windows Application Development', 
																						'OSX Application Development', 'Music', 'Product Design: UI/UX', 
																						'Product Design: Graphics (Photoshop/Illustrator/etc)', 'Marketing', 
																						'Statistics', 'Management');
						
			$tal_code = array('dev-web-front','dev-web-back','dev-mobile','dev-win','dev-osx',
														'art-music','art-design-uiux','art-design-graphics','marekting',
														'statistics','management');
			
			return str_replace($tal_code, $tal_human_readable, $str);
		}
	}
?>