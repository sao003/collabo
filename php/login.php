<?php
require("../config.php");
include 'PasswordHash.php';

function curPageURL() {
 $pageURL = 'http';
 if (!empty($_SERVER['HTTPS']) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

$curPageURL = curPageURL();

ob_start(); // start remembering as following

// Connect to the server and select the database
$connection = mysqli_connect("$host","$dbusername","$dbpassword",$dbname);
if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
}

// Take data from the form
$submitted_username = $_POST['inputEmail'];
$submitted_password = $_POST['inputPassword'];

// Prevent sql injection
$submitted_username = stripslashes($submitted_username);
$submitted_password = stripslashes($submitted_password);
$submitted_username = mysqli_real_escape_string($connection, $submitted_username);
$submitted_password = mysqli_real_escape_string($connection, $submitted_password);


if ($stmt= mysqli_prepare($connection, "SELECT * FROM $usertable WHERE username=?")) {
  mysqli_stmt_bind_param($stmt, 's', $submitted_username);
  mysqli_stmt_execute($stmt);

	mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password, $name, $talents);
	
  mysqli_stmt_store_result($stmt);

  $count = mysqli_stmt_num_rows($stmt);

  // username was found
  if ($count == 1) {
    mysqli_stmt_fetch($stmt) or die(mysqli_error());
    // check password using PasswordHash.php plugin
    if (validate_password($submitted_password, $hashed_password)) {
	    //logged in
      $_SESSION['user'] = $submitted_username;
      $_SESSION['userid'] = $id;

      header("Location: ../index.php");
      die("Redirecting to: ../index.php");
    } else {
      echo "Wrong username or password";
    }
  }
  // wrong pw
  else {
      echo "Wrong username or password";
  }

  mysqli_stmt_close($stmt);
}

// Close the connection to MySQL
mysqli_close($connection);

ob_end_flush(); // pair with ob_start();
?>
