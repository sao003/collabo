-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Nov 22, 2015 at 04:23 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `prjt`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
		  `id` int(11) NOT NULL,
		    `project_id` int(11) unsigned NOT NULL,
		      `user_id` int(11) unsigned NOT NULL
		) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `project_id`, `user_id`) VALUES
(15, 10, 5),
	(16, 11, 4),
	(17, 16, 4),
	(18, 10, 4);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
		  `id` int(11) NOT NULL,
		    `owner_id` int(11) unsigned NOT NULL,
		      `projectname` varchar(50) NOT NULL,
		        `projectdesc` varchar(3000) NOT NULL,
			  `talents_needed` varchar(600) NOT NULL
		) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `owner_id`, `projectname`, `projectdesc`,
		`talents_needed`) VALUES
(10, 4, 'Group Organizer', 'Help forming a group for a project!',
 'dev-web-front:::dev-web-back:::dev-mobile:::art-design-uiux:::marekting:::management'),
	(11, 5, 'Beer Bros', 'Find best beer near your town!',
	 'dev-web-front:::dev-web-back:::art-design-uiux:::art-design-graphics:::marekting:::statistics'),
	(12, 4, 'Data Analysis', 'Analyze data and get some good juice out of
	 it! In need of smart mathematicians and Designers for cool
	 visualization. Web dev can be useful?',
	 'dev-web-front:::art-design-graphics:::statistics'),
	(13, 6, 'iOS Action Game', 'I wanna develop a kool gamez with katz
	 flyin'' all over man. letz make kool stuffz plz. \n\nRequirements: \n1.
	 3 years of having katz\n2. 10 days of drawing katz\n3. COMPOSER NEEDED
	 SO BADLY\n4. It''s gonna fly anyways so no need business people.\n',
	 'dev-mobile:::art-music:::art-design-uiux:::art-design-graphics'),
	(14, 6, 'KATZ FLYIN'' iOS GAME', 'I wanna develop a kool gamez with katz
	 flyin'' all over man. letz make kool stuffz plz. Requirements:<br /><br
	 /> 1. 3 years of having katz <br /><br /> 2. 10 days of drawing katz
	 <br /><br /> 3. COMPOSER NEEDED SO BADLY <br /><br /> 4. It''s gonna
	 fly anyways so no need business people. ',
	 'dev-mobile:::art-music:::art-design-graphics'),
	(16, 5, 'Burrito Restaurant', 'Selling burritos more effectively.<br
	 /><br />Also need someone to make our theme song. ',
	 'art-music:::marekting:::management'),
	(17, 5, 'Terminal in Windows', 'Let''s transform CMD (Command Line) into
	 Terminal! I hate CMD experience so much. Even git bash works better
	 than regular CMD.', 'dev-win'),
	(18, 4, 'test', 'tttt', 'art-music');

-- --------------------------------------------------------

--
-- Table structure for table `username`
--

CREATE TABLE `username` (
		  `id` int(11) NOT NULL,
		    `username` varchar(100) NOT NULL,
		      `password` varchar(120) NOT NULL,
		        `name` varchar(100) NOT NULL,
			  `talents` varchar(2000) NOT NULL
		) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `username`
--

INSERT INTO `username` (`id`, `username`, `password`, `name`, `talents`) VALUES
(4, 'sean@ucsd.edu',
 'sha256:1000:a1glUQB9GTW7X93+83/sVgOpZBCYccOG:B12vz8//hP5OeMRLGfiyluHD3M521kWV',
 'Sean', 'dev-web-front:::dev-web-back:::dev-mobile:::art-design-uiux'),
	(5, 'john@uci.edu',
	 'sha256:1000:EjkETn616j0H45ia1tVlvoLViDL9ZIzA:KCx8/e2krSw8ngUTv4/O/SrteJ3uaFf3',
	 'John', 'dev-web-back:::dev-osx:::art-music:::art-design-uiux'),
	(6, 'harry@csulb.edu',
	 'sha256:1000:K6s47ZE2ZINWZejxQHbVdO1fhlWOC6i+:bdF0OlH03Cl+6nDX/veo9+5wgs3o0hCt',
	 'Harry', 'art-music');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

  --
  -- Indexes for table `projects`
  --
  ALTER TABLE `projects`
    ADD PRIMARY KEY (`id`),
      ADD KEY `owner_id` (`owner_id`),
        ADD KEY `talents_needed` (`talents_needed`(255));

--
-- Indexes for table `username`
--
ALTER TABLE `username`
  ADD PRIMARY KEY (`id`);

  --
  -- AUTO_INCREMENT for dumped tables
  --

  --
  -- AUTO_INCREMENT for table `members`
  --
  ALTER TABLE `members`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
    --
    -- AUTO_INCREMENT for table `projects`
    --
    ALTER TABLE `projects`
      MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
      --
      -- AUTO_INCREMENT for table `username`
      --
      ALTER TABLE `username`
        MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
